def echo (word)
  return "#{word}"
end

def shout (word, num = 1)
  arr = []
  num.times do
    arr.push(word.upcase)
  end
  arr.join(' ')
end

def repeat (word, num = 2)
  arr = []

  num.times do
    arr.push(word)
  end
  arr.join(' ')
end


def start_of_word (word, num = 1)
  str = ''
  i = 0
  while str.length < num
    str += word[i]
    i += 1
  end
  str
end


def first_word str
  words = str.split(' ')
  words[0]
end


def titleize str
  words = str.split(' ')
  newstr = []
  skip_words = 'and or the over a'.split(' ')
  words.each_with_index do |word, index|
    if index != 0 && skip_words.index(word)
      newstr.push(word)
    else
      newstr.push(word.capitalize)
    end
  end
  newstr.join(' ')
end
