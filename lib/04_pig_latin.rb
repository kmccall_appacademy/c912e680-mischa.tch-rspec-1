def translate str
  vowels = 'aeio'.split('')
  words = str.split(' ')
  new_str = []

  words.each do |word|
    if vowels.index(word[0])
      new_str.push(word + 'ay')
    else
      new_str.push(create_word_from_cons(word))
    end
  end
  new_str.join(' ')
end


def create_word_from_cons word
  vowels = 'aeio'.split('')
  new_word = ''

  word.split('').each_with_index do |letter, i|
    if vowels.index(letter) != nil
      return new_word += word.slice(i, word.length) + word.slice(0, i) + 'ay'
    end
  end
end
