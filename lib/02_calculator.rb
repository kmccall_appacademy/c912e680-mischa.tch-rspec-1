def add (num1, num2)
  return num1 + num2
end

def subtract (num1, num2)
  return num1 - num2
end

def sum arr
  sum = 0
  arr.each do |element|
    sum += element
  end
  return sum
end


def multiply (num1, num2)
  return num1 * num2
end

def power (num, power)
  return num ** power
end

def factorial num
 curr = num
 result = 1
 while curr != 1
   result *= curr
   curr -= 1
 end
 return result
end
